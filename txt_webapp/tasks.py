# -*- coding: utf-8 -*-
"""
Created on Aug 19, 2015

@author: Daniel
"""
from celery.app.base import Celery
from flask.app import Flask
from flask_cors.extension import CORS
from flask_cors.decorator import cross_origin
from tempfile import mkdtemp, gettempdir
from txtmining.controllers import controllers
from flask.globals import request
from werkzeug.utils import secure_filename
import os
from flask.json import jsonify
from zipfile import ZipFile
from flask.helpers import send_from_directory
from txtmining.controllers.controllers import MetadataController,\
    PreprocessingController, ReIndexController, TaxonfinderController,\
    PDFparserController, NetinetiController
from txtmining.tests.util import get_regex_folder
import codecs
import unicodecsv
import txtmining
from txtmining.document.text_indexer import RegexGroup, RegexIndexBuilder
from txtmining.converters.brat import BratConv
import shutil
from config import BACKEND
from config import BROCKER

app = Celery('tasks',
             backend=BACKEND,
             broker=BROCKER)
webapp = Flask(__name__)
cors = CORS(webapp)
webapp.config['CORS_HEADERS'] = 'Content-Type'
app.conf.update(
    CELERY_ACCEPT_CONTENT=['json'],
    CELERY_TASK_SERIALIZER='json',
    CELERY_RESULT_SERIALIZER='json',
)


def create_zip_from_dir(out_dir):
    zip_file = os.path.join(out_dir, 'work.zip')
    with ZipFile(zip_file, 'w') as z:
        for root, _, files in os.walk(out_dir):
            for f in files:
                ext = os.path.splitext(f)[1]
                if ext != '.zip':
                    save_as = os.path.join(root, f)
                    save_as = save_as.split("/")
                    save_as = save_as[-2:]
                    save_as = '/'.join(save_as)
                    z.write(os.path.join(root, f),
                            save_as)
    return zip_file


def create_file(input_data, file_name, output_dir=None):
    if output_dir is None:
        output_dir = mkdtemp()
    file_name = os.path.join(output_dir, file_name)
    with codecs.open(file_name, 'w', 'utf-8-sig') as o:
        o.write(input_data)
    return file_name


@app.task
def celery_parser_controller(parameters):
    tika_bin = os.getenv('TIKA_BIN', None)
    pdfbox_bin = os.getenv('PDFBOX_BIN', None)
    pdftotext_bin = os.getenv('PDF_TO_TEXT_BIN', None)
    out_dir = mkdtemp()
    controller = controllers.PDFparserController(out_dir, tika_bin,
                                                 pdfbox_bin, pdftotext_bin)
    if parameters['tika']:
        controller.apply(parameters['infile'], 'tika')
    if parameters['pdfbox']:
        controller.apply(parameters['infile'], 'pdfbox')
    if parameters['pdftotext']:
        controller.apply(parameters['infile'], 'pdftotext')
    # Create zip file
    return create_zip_from_dir(out_dir)


@app.task
def celery_meta_controller(parameters):
    tika_bin = os.getenv('TIKA_BIN', None)
    pdfinfo_bin = os.getenv('PDF_INFO_BIN', None)
    controller = MetadataController(tika_bin, pdfinfo_bin)
    return_val = list()
    if parameters['tika']:
        return_val.append(controller.apply(parameters['infile'],
                                           'tika'))
    if parameters['pdfinfo']:
        return_val.append(controller.apply(parameters['infile'],
                                           'pdfinfo'))
    return return_val


@app.task
def celery_preprocessing_controller(parameters):
    opennlp_bin = os.getenv('OPENNLP_BIN', None)
    models_resource = os.path.join(txtmining.__path__[0],
                                   'resources',
                                   'models')
    if parameters['lang'] == 'es':
        # load spanish model
        opennlp_model = os.path.join(models_resource,
                                     'es-iula.bin')
    elif parameters['lang'] == 'en':
        opennlp_model = os.path.join(models_resource,
                                     'en-sent.bin')
    controller = PreprocessingController(opennlp_bin, opennlp_model)
    correction_type = list()
    if parameters['dash']:
        correction_type.append('dash')
    if parameters['abrv']:
        correction_type.append('abrv')
    if parameters['onel']:
        correction_type.append('onel')
    if parameters['sent']:
        correction_type.append('sent')
    output = controller.apply(parameters['infile'],
                              correction_type)
    output = output.decode('UTF-8')
    text_file = create_file(output, parameters['infile'])
    text_dir = os.path.dirname(text_file)
    return create_zip_from_dir(text_dir)


@app.task
def celery_regular_expression_controller(parameters):
    regexp_file = get_regex_folder()
    regexp_file = os.path.join(regexp_file, 'regexp_NombreCientifico.re')
    controller = ReIndexController(regexp_file)
    output = controller.apply(parameters['infile'])
    output = [o + ('Lista',) for o in output]
    if parameters['listFile']['file']:
            # The user has attached a file with a list
            try:
                re = RegexGroup(parameters['listFile']['file'])
                re_output = RegexIndexBuilder(parameters['infile'],
                                              re.re).get_index()
                re_output = [o + ('Lista personalizada', )
                             for o in re_output]
                output += re_output
            except Exception as e:
                print(e)
    # Adding Taxonfinder
    if parameters['taxonfinder']:
        taxonfinder_controller = TaxonfinderController()
        taxonfinder_output = (taxonfinder_controller.
                              apply(parameters['infile']))
        taxonfinder_output = [o + ['Taxonfinder']
                              for o in taxonfinder_output]
        output += taxonfinder_output
    if parameters['netineti']:
        language = parameters['lang']
        netineti_controller = NetinetiController(language)
        netineti_output = (netineti_controller
                           .apply(parameters['infile']))
        netineti_output = [o + ('Netineti', )
                           for o in netineti_output]
        output += netineti_output
    with codecs.open(parameters['infile'], 'r', 'UTF-8') as f:
        file_lines = f.readlines()
    for i, val in enumerate(output):
        line_number = val[0]
        line = file_lines[line_number - 1]
        output[i] += (line,)
    return output


@app.task
def celery_whole_process_controller(parameters):
    tika_bin = os.getenv('TIKA_BIN', None)
    pdfbox_bin = os.getenv('PDFBOX_BIN', None)
    pdftotext_bin = os.getenv('PDF_TO_TEXT_BIN', None)
    opennlp_bin = os.getenv('OPENNLP_BIN', None)
    models_resource = os.path.join(txtmining.__path__[0],
                                   'resources',
                                   'models')
    if parameters['lang'] == 'es':
        # load spanish model
        opennlp_model = os.path.join(models_resource,
                                     'es-iula.bin')
    elif parameters['lang'] == 'en':
        opennlp_model = os.path.join(models_resource,
                                     'en-sent.bin')
    regexp_file = get_regex_folder()
    regexp_file = os.path.join(regexp_file, 'regexp_NombreCientifico.re')
    out_dir = mkdtemp()
    outputs = list()
    parsed_files = list()
    for infile in parameters['files']:
        # Check extensions to see if it is a PDF o TXT
        extension = os.path.splitext(infile)[1]
        if extension == '.pdf' or extension == '.doc':
            pdf_parser = PDFparserController(out_dir, tika_bin,
                                             pdfbox_bin, pdftotext_bin)
            parsed_file = pdf_parser.apply(infile,
                                           parameters['pdfparser'])
            print(parsed_file)
            parsed_file = parsed_file[parameters['pdfparser']+'_out']
            parsed_files.append(parsed_file)
            if parameters['preprocessing']:
                preprocessing = PreprocessingController(opennlp_bin,
                                                        opennlp_model)
                corrected_file = preprocessing.apply(parsed_file,
                                                     correction_type=['all'])
                corrected_file = corrected_file.decode('UTF-8')
                infile_name = os.path.splitext(infile)[0]
                infile_name = os.path.basename(infile_name)
                infile_name += '.txt'
                parsed_file = create_file(corrected_file,
                                          infile_name,
                                          output_dir=out_dir)
                parsed_files.pop()
                parsed_files.append(parsed_file)
        else:
            # move files to same directory
            dst = os.path.join(out_dir,
                               os.path.basename(infile))
            shutil.move(infile, dst)
            parsed_file = dst
            parsed_files.append(parsed_file)
        controller = ReIndexController(regexp_file)
        output = controller.apply(parsed_file)
        output = [o + ('Lista',) for o in output]
        if parameters['listFile']['file']:
            # The user has attached a file with a list
            try:
                re = RegexGroup(parameters['listFile']['file'])
                re_output = RegexIndexBuilder(parsed_file,
                                              re.re).get_index()
                re_output = [o + ('Lista personalizada', )
                             for o in re_output]
                output += re_output
            except Exception as e:
                print(e)
        if parameters['taxonfinder']:
            taxonfinder_controller = TaxonfinderController()
            taxonfinder_output = (taxonfinder_controller.apply(parsed_file))
            taxonfinder_output = [o + ['Taxonfinder']
                                  for o in taxonfinder_output]
            output += taxonfinder_output
        if parameters['netineti']:
            language = parameters['lang']
            netineti_controller = NetinetiController(language)
            netineti_output = netineti_controller.apply(parsed_file)
            netineti_output = [o + ('Netineti',)
                               for o in netineti_output]
            output += netineti_output
        with codecs.open(parsed_file, 'r', 'UTF-8') as f:
            file_lines = f.readlines()
        for i, val in enumerate(output):
            line_number = val[0]
            line = file_lines[line_number - 1]
            line = line.replace('\n', '')
            output[i] += (line,)
        outputs.append(output)
    if not parameters['multiple']:
        parsed_file = parsed_file.split("/")
        parsed_file = '/'.join(parsed_file[-2:])
        return {'array': outputs[0],
                'file': parsed_file}
    else:
        for id_file, parsed_file in enumerate(parsed_files):
            parsed_file_csv = os.path.splitext(parsed_file)[0]
            parsed_file_csv += '.csv'
            with open(parsed_file_csv, 'w') as f:
                csv_writer = unicodecsv.writer(f, encoding='utf-8')
                output = outputs[id_file]
                csv_writer.writerow([u'Número de línea',
                                     u'Nombre científico',
                                     u'Recurso',
                                     u'Texto'])
                for out in output:
                    # remove character position
                    out_without_position = [a for a in out
                                            if not (isinstance(a, tuple) or
                                                    isinstance(a, list))]
                    csv_writer.writerow(out_without_position)
        return create_zip_from_dir(out_dir)


@webapp.route('/single_file/PDFparserController',
              methods=['POST'])
@cross_origin()
def parser_controller():
    content = request.get_json()
    task = celery_parser_controller.delay(content)
    return jsonify({'taskid': task.task_id})


@webapp.route('/single_file/MetadataController',
              methods=['POST'])
@cross_origin()
def meta_controller():
    content = request.get_json()
    task = celery_meta_controller.delay(content)
    return jsonify({'taskid': task.task_id})


@webapp.route('/single_file/PreprocessingController',
              methods=['POST'])
@cross_origin()
def preprocessing_controller():
    content = request.get_json()
    task = celery_preprocessing_controller.delay(content)
    return jsonify({'taskid': task.task_id})


@webapp.route('/single_file/ReIndexController',
              methods=['POST'])
@cross_origin()
def regular_expression_controller():
    content = request.get_json()
    task = celery_regular_expression_controller.delay(content)
    return jsonify({'taskid': task.task_id})


@webapp.route('/single_file/WholeProcess',
              methods=['POST'])
@cross_origin()
def whole_process_controller():
    content = request.get_json()
    task = celery_whole_process_controller.delay(content)
    return jsonify({'taskid': task.task_id})


@webapp.route('/brat',
              methods=['POST'])
@cross_origin()
def brat_scinames():
    text = request.data
    dir_name = mkdtemp()
    file_path = os.path.join(dir_name, 'brat.txt')
    with codecs.open(file_path, 'w', 'UTF-8') as f:
        f.write(text.decode('UTF-8'))
    taxonfinder_controller = TaxonfinderController()
    taxonfinder_output = taxonfinder_controller.apply(file_path)
    bratted = BratConv.ann_format(file_path,
                                  taxonfinder_output,
                                  label="Nombre_cientifico")
    return jsonify(bratted)


@webapp.route('/upload_file',
              methods=['POST'])
@cross_origin()
def upload_file():
    f = request.files['file']
    filename = secure_filename(f.filename)
    output_dir = mkdtemp(prefix='tmpwebapp')
    output_file = os.path.join(output_dir, filename)
    f.save(output_file)
    return jsonify({'file_name': output_file})


@webapp.route('/check_task_status',
              methods=['POST'])
@cross_origin()
def check_task_status():
    content = request.get_json()
    task_id = content['taskid']
    res = app.AsyncResult(task_id)
    if not res.ready():
        return jsonify({'ready': False})
    else:
        file_created = res.result
        file_created = file_created.split("/")
        file_created = file_created[-2:]
        file_created = '/'.join(file_created)
        return jsonify({'ready': True,
                        'file': file_created})


@webapp.route('/check_task_status_array',
              methods=['POST'])
@cross_origin()
def check_task_status_array():
    content = request.get_json()
    task_id = content['taskid']
    res = app.AsyncResult(task_id)
    if not res.ready():
        return jsonify({'ready': False})
    else:
        array_result = res.result
        return jsonify({'ready': True,
                        'array': array_result})


@webapp.route('/download_file/<path:filename>')
@cross_origin()
def download_file(filename):
    return send_from_directory(gettempdir(), filename)
