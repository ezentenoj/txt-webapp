#!/usr/bin/env python
'''
Created on Jul 30, 2015

@author: Daniel
'''
from tasks import webapp
from paste.translogger import TransLogger
import cherrypy
import subprocess
import threading


class CeleryThread(threading.Thread):
    def run(self):
        error_message = ('Something went wrong when trying to '
                         'start celery. Make sure it is '
                         'accessible to this script')
        command = ['celery',
                   '-A',
                   'tasks',
                   'worker',
                   '--loglevel=info']
        try:
            _ = subprocess.check_output(command)
        except Exception:
            raise Exception(error_message)


def run_server():
    # start celery
    celery_thread = CeleryThread()
    celery_thread.start()
    app_logged = TransLogger(webapp)
    cherrypy.tree.graft(app_logged, '/')
    cherrypy.config.update({
        'engine.autoreload.on': True,
        'log.screen': True,
        'server.socket_port': 5000,
        'server.socket_host': '0.0.0.0',
        })
    cherrypy.engine.start()
    cherrypy.engine.block()


if __name__ == '__main__':
    run_server()
