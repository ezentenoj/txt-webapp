/**
 * 
 */

(function(){
	
	angular.module('text-mining.config', [])
		.constant('APP_PROTOCOL', 'http')
		.constant('APP_IP', 'localhost')
		.constant('APP_PORT', '5000')
	
})();