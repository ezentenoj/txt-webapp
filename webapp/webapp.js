/**
 * 
 */

var render = function(){
			componentHandler.upgradeAllRegistered();
		};

(function(){
	
	var app = angular.module('text-mining', ['ngFileUpload', 'text-mining.config']);
	
	app.controller('InitTaskCtrl', ['$log', '$http',
	                                '$scope', '$timeout',
	                                '$location', '$rootScope',
	                                'Upload', 'APP_PROTOCOL', 'APP_IP', 'APP_PORT',
	                                function($log, $http,
	                                		$scope, $timeout,
	                                		$location, $rootScope,
	                                		Upload, appProtocol, appIP, appPort){
		
		
		/////////////////Celery
		$scope.task = {
				'id': null,
				'fileCreated': null
		};
		
		$scope.url = appProtocol+'://'+appIP+':'+appPort+'/';
		
		var checkStatus = function(){
			$http.post($scope.url+'check_task_status',
					{'taskid': $scope.task.id}).
					then(function(response){
						var ready = response.data.ready;
						if (!ready)
							$timeout(checkStatus, 1000 * 5);
						else{
							$scope.task.fileCreated = response.data.file;
							$location.hash("");
							$timeout(downloadFile(), 200);
						}
					},
					function(response){
						setError();
					});
		};
		
		var checkStatusArray = function(obj){
			$http.post($scope.url+'check_task_status_array',
					{'taskid': $scope.task.id}).
					then(function(response){
						var ready = response.data.ready;
						if (!ready)
							$timeout(checkStatusArray, 1000 * 5, true, obj);
						else{
							obj.ready = true;
							obj.array = response.data.array;
							$location.hash("");
							$timeout(render, 10);
						}
					},
					function(response){
						setError();
					});
		};
		
		var downloadFile = function(){
			location.href = $scope.url+'download_file/' + $scope.task.fileCreated;
		}
		
		
		//*********************//
		
		
		$scope.step = 0;
		$scope.path = null;
		$scope.emptyFileError = false;
		
		$rootScope.$on('$locationChangeStart', function(event, newUrl, oldUrl){
			if($location.path() != ''){
				if ($location.hash() == 'working')
					$scope.step = -1;
				if ($location.hash() == ''){
					$scope.path = $location.path().split("/");
					$scope.path.shift();
					switch($scope.path[0]){
					case 'single_file':
						$scope.step = 1;
						if ($scope.path.length > 1)
							$scope.singleFile.gotoOption($scope.path[1]);
						else
							$scope.singleFile.gotoOption(0);
						break;
					case 'batch':
						$scope.step = 2;
						break;
					default:
						$scope.step = -10;
					}
				}
				if ($location.hash() == 'error')
					$scope.step = -10;
				$timeout(render, 10);
			}
		});
		
		$scope.clearError = function(){
			$location.hash("");
		}
		
		var setError = function(){
			$location.hash("error");
		};
		
		var setFileEmptyError = function(){
			$scope.emptyFileError = true;
			$timeout(function(){
				$scope.emptyFileError = false;
			}, 1000 * 5);
		};
		
		//////////////Single file_option
		
		$scope.singleFile = {};
		$scope.singleFile.optionTxts = ["Seleccione una opción",
		                                "Extraer texto de archivo",
		                                "Extraer metadatos de PDF",
		                                "Corregir archivo de texto",
		                                "Buscar nombres científicos"]
		$scope.singleFile.option = 0;
		$scope.singleFile.optionTxt = $scope.singleFile.optionTxts[0];
		$scope.singleFile.setOptionTxt = function(option){
			$scope.path = [$scope.path[0]];
			$scope.path.push('' + option);
			var path = $scope.path.join("/");
			path = "/" + path;
			$location.path(path);
		};
		
		$scope.singleFile.gotoOption = function(option){
			$scope.singleFile.option = option;
			$scope.singleFile.optionTxt = $scope.singleFile.optionTxts[option];
			$timeout(render, 10);
		}
		
		
		//******************/
		
		//////////////PDFParserController
		$scope.pdfparser = {
				'tika': true,
				'pdfbox': true,
				'pdftotext': true,
				'infile': null
		};
		
		$scope.pdfparser_process = function(){
			if ($scope.pdfparser.infile == null){
				setFileEmptyError();
				return 0;
			}
			$location.hash("working");
			$timeout(render, 10);
			Upload.upload({
				url: $scope.url+'upload_file',
				file: $scope.pdfparser.infile
			}).then(function(response){
				$scope.pdfparser.infile = response.data.file_name;
				$http.post($scope.url+'single_file/PDFparserController',
						$scope.pdfparser).
						then(function(response){
							$scope.task.id = response.data.taskid;
							$timeout(checkStatus, 1000 * 5);
						},
						function(response){
							setError();
						});
			},
			function(response){
				setError();
			});
			
		
		};
		
		
		//*******************/
		
		
		
		//////////////MetadataController
		$scope.metadata = {
				'tika': true,
				'pdfinfo': true,
				'infile': null,
				'ready': false,
				'array': null,
				'filename': null
		};
		
		$scope.metadata_process = function(){
			if ($scope.metadata.infile == null){
				setFileEmptyError();
				return 0;
			}
			$scope.metadata.filename = $scope.metadata.infile.name;
			$location.hash("working");
			$timeout(render, 10);
			Upload.upload({
				url: $scope.url+'upload_file',
				file: $scope.metadata.infile
			}).then(function(response){
				$scope.metadata.infile = response.data.file_name;
				$http.post($scope.url+'single_file/MetadataController',
						$scope.metadata).
						then(function(response){
							$scope.task.id = response.data.taskid;
							$timeout(checkStatusArray, 1000 * 5, true, $scope.metadata);
						},
						function(response){
							setError();
						});
			}, function(response){
				setError();
			});
			
		
		};
		
		
		
		
		//*******************/
		
		
		
		//////////////PreprocessingController
		$scope.preprocessing = {
				'dash': true,
				'abrv': true,
				'onel': true,
				'sent': true,
				'infile': null,
				'lang': 'es'
		};
		
		$scope.preprocessing_process = function(){
			if ($scope.preprocessing.infile == null){
				setFileEmptyError();
				return 0;
			}
			$location.hash("working");
			$timeout(render, 10);
			Upload.upload({
				url: $scope.url+'upload_file',
				file: $scope.preprocessing.infile
			}).then(function(response){
				$scope.preprocessing.infile = response.data.file_name;
				$http.post($scope.url+'single_file/PreprocessingController',
						$scope.preprocessing).
						then(function(response){
							$scope.task.id = response.data.taskid;
							$timeout(checkStatus, 1000 * 5);
						},
						function(response){
							setError();
						});
			},
			function(response){
				setError();
			});
			
		
		};
		
		
		//*******************/
		
		
		
		
		//////////////ReIndexController
		$scope.regularExpression = {
				'infile': null,
				'ready': false,
				'taxonfinder': true,
				'netineti': true,
				'filename': null,
				'array': null,
				'lang': 'es',
				'listFile': {
					'screenName': null,
					'file': null
				},
		};
		
		$scope.regularExpression_process = function(){
			if ($scope.regularExpression.infile == null){
				setFileEmptyError();
				return 0;
			}
			$location.hash("working");
			$timeout(render, 10);
			$scope.regularExpression.filename = $scope.regularExpression.infile.name;
			Upload.upload({
				url: $scope.url+'upload_file',
				file: $scope.regularExpression.infile
			}).then(function(response){
				$scope.regularExpression.infile = response.data.file_name;
				$http.post($scope.url+'single_file/ReIndexController',
						$scope.regularExpression).
						then(function(response){
							$scope.task.id = response.data.taskid;
							$timeout(checkStatusArray, 1000 * 5, true, $scope.regularExpression);
						},
						function(response){
							setError();
						});
			},
			function(response){
				setError();
			});
			
		
		};
		
		$scope.regularExpressionUploadList = function(files, errFiles){
			angular.forEach(files, function(file){
				$scope.regularExpression.listFile.screenName = file.name;
				Upload.upload({
					url: $scope.url + 'upload_file',
					file: file
				}).then(function(response){
					$scope.regularExpression.listFile.file = response.data.file_name;
				},
				function(response){
					setError();
				});
			});
		};
		
		
		//*******************/
		
		
		
		
		//////////////Whole process
		
		$scope.wholeProcess = {
				'lang': 'es',
				'pdfparser': 'tika',
				'preprocessing': true,
				'taxonfinder': true,
				'netineti': true,
				'infile': null,
				'filename': null,
				'array': null,
				'ready': false,
				'multiple': false,
				'files': [],
				'listFile': {
					'screenName': null,
					'file': null
				},
		};
		
		$scope.wholeProcessUploadList = function(files, errFiles){
			angular.forEach(files, function(file){
				$scope.wholeProcess.listFile.screenName = file.name;
				Upload.upload({
					url: $scope.url + 'upload_file',
					file: file
				}).then(function(response){
					$scope.wholeProcess.listFile.file = response.data.file_name;
				},
				function(response){
					setError();
				});
			});
		};
		
		$scope.wholeProcess_process = function(){
			if ($scope.wholeProcess.infile == null){
				setFileEmptyError();
				return 0;
			}
			$location.hash("working");
			$timeout(render, 10);
			if (!$scope.wholeProcess.multiple){
				$scope.wholeProcess.filename = $scope.wholeProcess.infile.name;
				Upload.upload({
					url: $scope.url+'upload_file',
					file: $scope.wholeProcess.infile
				}).then(function(response){
					$scope.wholeProcess.files.push(response.data.file_name);
					$http.post($scope.url+'single_file/WholeProcess',
							$scope.wholeProcess).
							then(function(response){
								$scope.task.id = response.data.taskid;
								$timeout(checkStatusArray, 1000 * 5, true, $scope.wholeProcess);
							},
							function(response){
								setError();
							});
				},
				function(response){
					setError();
				});
			}
			else{
				var total_uploaded = 0;
				angular.forEach($scope.wholeProcess.infile, function(file){
					Upload.upload({
						url: $scope.url+'upload_file',
						file: file
					}).then(function(response){
						total_uploaded++;
						$scope.wholeProcess.files.push(response.data.file_name);
						if (total_uploaded == $scope.wholeProcess.infile.length){
							$http.post($scope.url+'single_file/WholeProcess',
									$scope.wholeProcess).
									then(function(response){
										$scope.wholeProcess.files = [];
										$scope.wholeProcess.infile = null;
										$scope.task.id = response.data.taskid;
										$timeout(checkStatus, 1000 * 5, true, $scope.wholeProcess);
									},
									function(response){
										setError();
									});
						}
					}, function(response){
						setError();
					});
				});
			}
			
		
		};
		
		$scope.wholeProcess_reset = function(){
			$scope.wholeProcess.ready = false;
			$scope.wholeProcess.array = null;
			$scope.wholeProcess.files = null;
			$scope.wholeProcess.files = [];
			$scope.wholeProcess.infile = null;
		}
		
		
		
		//******************/
		
		
	}]);
	
})();